===========
Unprocessed
===========

Repo Naming
===========

esp-meta-doc
   this documentation

esp-<project>-verificator
   testbed wrapping

esp-<project>-doc
   The documentation of the project including requirements with refs
   and architecture and also bdd test specs

esp-<project>-manual
   The documentation of the project plan and engineering approach

esp-<project>-fw
   The firmware application(s) and board adaptation(s),
   i.e. the main target

We believe
==========

Community sharing 
   Community maintained open source technology is cost efficient for companies and the society.
   Provides a maximum of software quality

   State of the art RTOS 

Automated and Secure Firmware Update
   is a mandatory feature to cope with upcoming hugh device variety and distrution

Hardware in a Loop (HIL) based firmware verification
   is mandatory to be able maintaining firmware product lifecycle beyond
   hardware/electronics product lifecycle

Separation of the binding to Hardware/Electronics binding from the business logic 
   reduces the dependency between hardware and software makes porting easy

Zero Bug Mentality
   is the most cost efficient approach to deal with exploding software complexity

Vision
======

We believe that specifically for Embedded Software

* Unit Testing and a Test Coverage provice good confidence of a correct functional implementation.
  Potential mis-behavior are most likely timing issues, asynchronous issues

* CI is great to not forget to test "exotic" variants.

* The ease of porting/migrating to slightly different hardware loses the coupling between hardware and software

* Investment in multi target / multi application / multi toolchain configuration management pays off in higher
  maintainability and stronger code stability

* Investment into evalboard/raspberry controlled HIL testing adds only little cost/effort to the project
  and gains a lot of predictability in delivery and verified behavior to the embedded software

* Due to the complexity of nowerdays systems, only a zero bug tolerance leads let us survive the maintenance hell

* The expectation of chaning requirements is not a reason to not write them down.



Key Approaches
==============

Configuration Management
   via git repos and google-repos

   versioning stamped in source are git tags

End to End Focus
   A task is done only if

   * it is passed all unit tests
   * it the increment is deployed to all defined targets
   * it passed integration tests (BDD) on target i.e. the increment is verified on target
     to maintain its quality

Hardware decoupled Business Logic
   Business Logic can evolve completely independent from target hardware

   We separate porting the (business application) to another hardware a different task
   than producing new business logic.

   There is a hardware abstraction layer in the software architecture that supports that
   separation.

   At integration test level specific hardware and specific business logic is combined via
   configuration Management

Fully automated verification from the very beginning
   Embedded system delivery is always a combination of specific hardware/electronics and Firmware.

   BDD specified Component and HW-Integration tests address this.

Facilitate Standards, Community Protocols, Open source Implementation
   Take advantage of community efforts (as well as contribute through out the projects)

   * end up with high quality and high quantity delivery

Documentation as Code
   We use the :term:`Sphinx` documentation system to lay down requirements, sw-architecture, and tests
   such that everything is under the same version control and can be related, traced 
   to the implemented software

Infrastructure as Code
   We maintain our toolchains build tools as code (e.g. via Dockerfiles, ci-files etc) such that everything
   can be:

   * reproduced (without operational errors)
   * be subject of configuration Management
   * can be bootstrapped for any new software engineer or test engineer

Bootstrap the project
   Have an empty application ready almost automatically

Secure and Safe Firmware Update
   Mandatory 

Process and Engineering Approaches
==================================

* Establish requirements also for HW-Targets HILS
* Semantic versioning 0.x during development
* we do not fix bugs on old versions but on the main line
* harmonized terminology for targets,
* formalize by conventions
* maintain HIL(s) for support period
* tag requirements (app, verificator, primary, derived)
* trace requirements
* definitions of ready (submitted request/order)
* definitions of done (delivered, integrated, documented, tested)
* Flow visualization (Backlog, Sprint boards, weekly schedule)
* Release: after tagging, automated release notes, from master only
* git code review, no diamonds, simple branch model, multi repos, google repo/ git submudules
