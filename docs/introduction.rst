============
Introduction
============

.. include :: glossary.rst-incl

ESP is ...
==========

Embedded Software Production can be described as:

#. Having a Production system for Embedded Software in place

   Toolchains in docker container, headless build, ci/cd - service, configuration managament system via git,
   google repo, a branching model, a tagging model and a versioning scheme, a requirement management scheme,
   a HIL-simulator

#. **Process Mini-Orders sequentially**

   Mini-Orders are either business-feature requests, bug-fix requests or Hardware adaptation requests.
   A Mini-Order is a |story| that might refer to requirements (changes)

#. **Batch Size One**

   Every Mini-Order is different. Toolchains, production details (architecture update, doc update, implementation,
   unit testing, quality verification procedures) adapt specifically to the individual Mini-Order.

#. **Documented Production Process**

   Production control documents are maintained. I.e. those are software architecture update, test spec update,
   CI reports are generated. all changes are tracked in git version control and are revisionized.
   Documentation as code is applied.

#. **Quality Gating**

   Intrinsically into the software production line quality gates have to be passed. Those are
   source code review (against coding guidelines), applied source code linting metrics,
   unit tests and coverage metrics, full BDD test case coverage against requirements.

#. **Delivery into Composed Product**

   The produced increment of software is always integrated into the composed product and also tested/
   verified as part of the Composed Product.



Firmware Standard Features
==========================

* every |firmware| identifyable i.e. have a name and a unique version. The name expresses the |target| and the |application|.
* every |target| have to have a console. (even if it is empty) for logging/tracing information.
* every |firmware| can be securely and safely updated via USB (DFU), IP-network, UART, CAN or SPI. There is no JTAG
  flashing required.

Quality Attributes of Firmware
==============================

* Up-to-date requirement document
* Up-to-date software architecture document
* Up-to-date BDD test specification referencing requirements
* BDD specs are used to "HIL-test" the application


Efficiency Attributes
=====================

* always something running. E2E focus
* easy bootstrapping: coockicutter templates, docker toolchains, tutorials
* processes and terminology by convention
* Business Logic decoupled from Hardware binding - easy survival with eval hardwareware until prototype gets available
* HIL-simulator updates synchronized with app feature development to meet the q-gates


