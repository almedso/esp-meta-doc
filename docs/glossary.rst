Glossary
========

.. include :: glossary.rst-incl

.. glossary ::

   application
      is the implementation of the business logic and the business behavior
      an embedded device shall perform. It abstracts from the very specific
      hardware.

   board
      A synonym that refers to a single MCU and it's specific connected peripherals chips including those IC's that
      are fully controlled by the firmware.

      Most often this is done on a single |PCB| 

   firmware
      Is an application that bound to a specific target.
      And therefore only runs on that specific target.

   HIL
      Hardware in a loop
      see https://en.wikipedia.org/wiki/Hardware-in-the-loop_simulation
      see https://de.wikipedia.org/wiki/Hardware_in_the_Loop

   lifecycle
      see https://en.wikipedia.org/wiki/Life_cycle.

      If talking about firmware lifecycle the software development lifecycle is meant.
      If talking about electronics/and hardware rather the product lifecycle is in mind.

   PCB
      Printed Circuit Board

   Sphinx
      Sphinx is a tool that makes it easy to create intelligent and beautiful documentation.
      It was originally created for the Python documentation, and it has excellent facilities for the documentation
      of software projects in a range of languages.

   story
      or also user story is a description of a piece of work in SCRUM context.
      see https://www.mountaingoatsoftware.com/agile/user-stories

   RST
      RST is an easy-to-read, what-you-see-is-what-you-get plain text markup syntax and parser system.
      It is useful for in-line program documentation (such as Python docstrings), for quickly creating simple web pages,
      and for standalone documents. RST is designed for extensibility for specific application domains.
      The RST parser is a component of Docutils.

   target
      is the specific hardware where an application shall run on.
      I.e. it a |board|.
