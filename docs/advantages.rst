==========
Advantages
==========

.. include :: glossary.rst-incl

Maintainable
============

* Source Code Review
* Test Driven Detailed Design
* BDD tested component
* Coding guidelines

Lean Process
============

* Requirements up-to-date
* Architecture Docu up-to-date
* Customer docu / API docu up-to-date
* Releasing is tagging on master

Predictable Output
==================

* progress visualized
* quality visualized

Scaling and State of Art Tooling
================================

* multi git repository eco-system
* cloud enabled
  * dockerized toolchains
  * dockerized target emulation
  * ci integration
* IOT enabled
  * secured communication protocols
  * secure and safe firmware update


Separate Hardware from Function
===============================

* Hardware development independent from Software Production
* Migration to new board - low risk issue, pure technical nature

Adaptable Output
================
* ordering small increments
* little lead time
