ESP Meta Documentation
======================

We envision the **embedded software** development as **production** (i.e. implementation)
of atomic embedded software elements (batch size one)
including conrresponding quality assurance software elements
(i.e. unit tests, and HIL-simulator extensions controllable by BDD feature files)
plus the assemply of those elements into a target product and arbitrary products for
board bring up, system testing, or factory configuration/calibration.

We focus on fully operable, end to end tested intermediate states that finally lead
to the desired product incorporating the desired complete feature set. 

** So this is a low enginerring risk and cost efficient Embedded Software Model that **

* copes well with requirement changes
* delivers high quality maintainable software increlentally
* abstracts functionality from concreate hardware




.. toctree::
   :hidden:

   docs/introduction
   docs/advantages
   docs/order-lifecycle

   docs/unprocessed

   docs/glossary
   docs/references


