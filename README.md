# Embedded Software Production - Meta Documentation Project

This is a Sphinx project and the main entry point to
follow the Embedded Software Production philosophy.

## Getting Started

Prerequisites:

* have python installed and sphinx installed
* alternatively use docker container **almedso/sphinx-doc-builder** and perform
  the following actions inside the container

Install hieroglyph, themes etc. :

```
pip install -r requirements.txt
```

Create the docu:

```
sphinx-build -b html  .  _build
```

##  Published documentation

The documentation is published via gitlab pages at

https://almedso.gitlab.io/esp-meta-doc

relecting the latest state of master branch.

## References

* https://pypi.org/project/hieroglyph/
* https://sphinx-rtd-theme.readthedocs.io/en/latest/

## License

(c) Copyright 2019 almedso GmbH
